import Vue from "vue";
import Router from "vue-router";
import Dashboard from "./views/Dashboard";
import LastQuotations from "./views/LastQuotations";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: LastQuotations
    },

    {
      path: "/dashboard",
      name: "dashboard",
      component: Dashboard
    },

    {
      path: "/last-quotations",
      name: "last-quotations",
      component: LastQuotations
    }
  ]
});
